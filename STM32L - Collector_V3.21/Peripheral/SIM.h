#ifndef  _SIM_H_
#define	 _SIM_H_

#define		SIM800C_ENABLE_TYPE		GPIOB		//δʹ��
#define		SIM800C_ENABLE_PIN		GPIO_Pin_14

#define		SIM_PWR_ON()			GPIO_SetBits(SIM800C_ENABLE_TYPE, SIM800C_ENABLE_PIN);
#define		SIM_PWR_OFF()			GPIO_ResetBits(SIM800C_ENABLE_TYPE, SIM800C_ENABLE_PIN);


void SIM800CPortInit(void);

void SIM800CProcess(unsigned short nMain10ms);

void SIM_Send_Alive(void);

unsigned int SIM_Report_Data(void);

void OnSIMComm(unsigned char CommData);

void SetSim800cReadyFlag( unsigned char ucFlag );

unsigned char GetSim800cReadyFlag( void );

float GetLongitude(void);

float GetLatitude(void);


#endif

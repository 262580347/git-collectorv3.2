/**********************************
说明：GPRS汉枫模块通信
	  发送心跳包和数据包，收到应答后断电休眠
		
作者：关宇晟
版本：V2017.11.20
***********************************/
#include "main.h"
#include "GPRS.h"

#define COM_DATA_SIZE	256

void GPRS_Port_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	GPIO_InitStructure.GPIO_Pin = GPRS_PWR_PIN ;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPRS_PWR_TYPE, &GPIO_InitStructure);

}

void GPRS_Process(unsigned short nMain10ms)
{
	static unsigned short s_LastTime;
	static unsigned char Run_Once = FALSE, s_State = 0, s_RetryCount = 0, s_OverTimeCount = 0;
	static unsigned int s_RTCSumSecond = 0, s_AliveRTCTime = 0xfffe0000;
	SYSTEMCONFIG *p_sys;
	static unsigned char s_ReportConfigFlag = FALSE;
	static CLOUD_HDR *hdr;
	unsigned char send_buf[128] = {0};
	p_sys = GetSystemConfig();
	

	if(Run_Once == FALSE)
	{
		Run_Once = TRUE;
		
		GPRS_Port_Init();
		GPRS_PWR_OFF();
		delay_ms(1000);
		GPRS_PWR_ON();
		SetTCProtocolRunFlag(TRUE);
		USART2_Config(115200);
		s_LastTime = GetSystem10msCount();
		
		
	}
	
	switch(s_State)
	{
		case 0:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 1200)	//等待汉枫模块建立TCP连接
			{
				Clear_Uart2Buff();
				s_State++;
				SetTCProtocolRunFlag(TRUE);
				s_LastTime = GetSystem10msCount();
			}
		break;
		
		case 1: //等待传感器测量完成
			if(DifferenceOfRTCTime(GetRTCSecond(), s_AliveRTCTime) >= p_sys->Heart_interval)	
			{
				s_AliveRTCTime = GetRTCSecond();
				Mod_Send_Alive(GPRS_COM);
				delay_ms(20);
				if(s_ReportConfigFlag == FALSE)
				{
					s_ReportConfigFlag = TRUE;
					ReportSystemConfig();
				}
					
			}
				
			if(g_485SensorGetFlag && g_PowerDetectFlag)
			{
				if(g_TCRecStatus.DataAckFlag == FALSE)
				{
					Mod_Report_Data(GPRS_COM);
				}
				s_LastTime = GetSystem10msCount();
				s_RetryCount++;
				s_State++;
				if(s_RetryCount >= 10)
				{
					u1_printf("\r\n Over time can't connect,reboot\r\n");
					while (DMA_GetCurrDataCounter(DMA1_Channel4));
					Sys_Soft_Reset();
				}	
			}			
		break;
		
		case 2://等待应答包
			if(g_TCRecStatus.DataAckFlag)	//收到心跳包和数据应答包
			{
				s_State++;
				s_RetryCount = 0;
				s_OverTimeCount = 0;
				s_LastTime = GetSystem10msCount();
				g_TCRecStatus.DataAckFlag = FALSE;
				s_RTCSumSecond = GetRTCSecond();	
				SetLogErrCode(LOG_CODE_SUCCESS);
				if(p_sys->LowpowerFlag == 0)
				{
					s_State = 3;	//低功耗设备
					WriteCommunicationSuccessCount();
					WriteCommunicationCount();
					SetTCProtocolRunFlag(FALSE);
				}
				else if(p_sys->LowpowerFlag == 1)
				{
					s_State = 6;	//非低功耗设备
				}
			}
			else //等待应答包超时
			{
				if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 400)
				{
					s_State--;
					s_LastTime = GetSystem10msCount();
					s_OverTimeCount++;
					
					p_sys = GetSystemConfig();
					if(s_OverTimeCount >= p_sys->Retry_Times+2)
					{
						SetLogErrCode(LOG_CODE_NOSERVERACK);
						u1_printf(" Reported Fail\r\n");
						s_OverTimeCount = 0;
						if(p_sys->LowpowerFlag == 0)
						{
							s_State = 3;	//低功耗设备
						}
						else if(p_sys->LowpowerFlag == 1)
						{
							s_State = 6;	//非低功耗设备
						}
						s_LastTime = GetSystem10msCount();
						g_TCRecStatus.DataAckFlag = FALSE;
						s_RTCSumSecond = GetRTCSecond();
						WriteCommunicationCount();
					}
				}		
			}
		break;
				
		case 3:
			if(GetServerConfigCmdFlag() == TRUE)
			{
				SetServerConfigCmdFlag(FALSE);
				s_LastTime = GetSystem10msCount();
			}
			
			if(GetCmdCloseSocket() == FALSE)
			{
				if(CalculateTime(GetSystem10msCount(),s_LastTime) >= 200)		//延时1s等待GPRS下行数据
				{
					u1_printf("\r\n  Wait server over time, sleep\r\n");
					s_RTCSumSecond = GetRTCSecond();
					s_State++;
					s_LastTime = GetSystem10msCount();
					StoreOperationalData();
				}
			}
			else if(GetCmdCloseSocket())		//收到关闭Socket指令，可以休眠
			{
				u1_printf("\r\n Rec server ack,sleep\r\n");
				s_RTCSumSecond = GetRTCSecond();
				s_State++;
				s_LastTime = GetSystem10msCount();
				SetCmdCloseSocket(FALSE);
				
				StoreOperationalData();
			}
		break;	

		case 4:
			if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSumSecond) >= p_sys->Data_interval)		//达到上报数据时间点
			{
				s_RTCSumSecond = GetRTCSecond();			
				SetOnline(FALSE);
				s_RetryCount = 0;
				Clear_Flag();			//清传感器标志，开启传感器检测
				SetTCModuleReadyFlag(FALSE);
				s_State++;
				SetLogErrCode(LOG_CODE_CLEAR);	
			}		
			else			//未到上报时间，继续休眠
			{
				if(GetTCModuleReadyFlag())
				{
					SetStopModeTime(TRUE, STOP_TIME);	
				}
				else
				{
					SetStopModeTime(TRUE, ERR_STOP_TIME);	
				}
					
			}		
		break;
			
		case 5:		//重新上电汉枫模块
			GPRS_Port_Init();
			GPSPowerOff();
			GPRS_PWR_ON();
			delay_ms(500);
			
			SetTCProtocolRunFlag(TRUE);
			USART2_Config(115200);
			u1_printf("\r\n Eport Power On\r\n");
			s_LastTime = GetSystem10msCount();
			s_State = 0;
		break;	

		case 6:
			if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSumSecond) >= p_sys->Data_interval)		//达到上报数据时间点
			{
				s_RTCSumSecond = GetRTCSecond();			
				Clear_Flag();			//清传感器标志，开启传感器检测
				s_State = 1;
			}	
		break;
	}
	

}


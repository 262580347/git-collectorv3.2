#ifndef  _SIM800C_H_
#define	 _SIM800C_H_	

#define		SIM800C_ENABLE_TYPE		GPIOA		//δʹ��
#define		SIM800C_ENABLE_PIN		GPIO_Pin_11

#define		SIM_PWR_ON			GPIO_SetBits(SIM800C_ENABLE_TYPE, SIM800C_ENABLE_PIN);
#define		SIM_PWR_OFF			GPIO_ResetBits(SIM800C_ENABLE_TYPE, SIM800C_ENABLE_PIN);


void SIM800CPortInit(void);

void SIM800CProcess(unsigned short nMain10ms);

void SIM_Send_Alive(void);

unsigned int SIM_Report_Data(void);

void OnSIMComm(unsigned char CommData);
	
unsigned char GetSim800cReadyFlag( void );

void SetSim800cResetFlag( unsigned char ucData );

void SetSim800cReadyFlag( unsigned char ucFlag );
#endif



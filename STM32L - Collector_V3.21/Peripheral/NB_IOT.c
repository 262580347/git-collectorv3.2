#include "main.h"
#include "NB_IOT.h"

#define	NB_OK			0x01
#define	NB_OVERTIME		0x02
#define	NB_ERROR		0x03
#define	NB_MESSAGE		0x04

#define		MAX_NB_BUF  500

static unsigned char NB_RxdBuff[200];

unsigned char NB_SendCmd(char *Cmd, char *Result, unsigned short Timeout, unsigned char Retry, unsigned short WaitTime);

static unsigned char s_Signal = 0;

static unsigned char s_STM32StopFlag = FALSE;

static unsigned char s_AlivePackFlag = FALSE;

void NB_Reset(void)
{
	NB_RESET_LOW();
	delay_ms(120);
	NB_RESET_HIGH();
	delay_ms(120);
}

void NB_Wakeup(void)
{
	NB_WAKEUP_LOW();
	delay_ms(300);
	NB_WAKEUP_HIGH();
}

void NB_PowerKey(void)
{
	NB_POWERKEY_LOW();
	delay_ms(1100);
	delay_ms(1100);
	NB_POWERKEY_HIGH();
	delay_ms(200);
}

void NB_PortInit(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
			
	GPIO_InitStructure.GPIO_Pin = NB_EN_PIN ;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(NB_EN_TYPE, &GPIO_InitStructure);

	GPIO_SetBits(NB_EN_TYPE,NB_EN_PIN);
	
	GPIO_InitStructure.GPIO_Pin = NB_POWERKEY_PIN | NB_WAKEUP_PIN | NB_RESET_PIN;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	GPIO_ResetBits(NB_POWERKEY_TYPE,NB_POWERKEY_PIN);
	GPIO_ResetBits(NB_WAKEUP_TYPE,NB_WAKEUP_PIN);
	GPIO_ResetBits(NB_RESET_TYPE,NB_RESET_PIN);

}

static void WakeUpNBModules(void)
{
	NB_Wakeup();
	u3_printf("AT\r\n");
	delay_ms(200);
}

static unsigned char NB_SendCmd(char *Cmd, char *Result, unsigned short Timeout, unsigned char Retry, unsigned short WaitTime)
{
	static unsigned char s_State = 1, ErrCount = 0;
	static u16 s_LastTime = 0;
	u8 s_return = 0;
	
	switch(s_State)
	{
		case 1:
			u3_printf("%s", Cmd);
//			u1_printf("%s", Cmd);
			s_LastTime = GetSystem10msCount();
			if(Retry == 0)
			{
				s_return = NB_OK;
			}
			else
			{
				s_State++;
			}
		break;
		
		case 2:
			if((strstr((const char *)g_USART3_RX_BUF, "OK")) != NULL)
			{
				s_State = 4;
				s_LastTime = GetSystem10msCount();
				if(WaitTime == 0)
				{
					s_State = 1;
					ErrCount = 0;
					s_return = NB_OK;
				}
			}
			else if((strstr((const char *)g_USART3_RX_BUF, Result)) != NULL)
			{
				s_State = 4;
				s_LastTime = GetSystem10msCount();
				if(WaitTime == 0)
				{
					s_State = 1;
					ErrCount = 0;
					s_return = NB_MESSAGE;
				}
			}
			else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= Timeout) 
			{
				s_State++;
				ErrCount++;
			}
		break;
			
		case 3:
			if(ErrCount < Retry)
			{
				s_State = 1;
			}
			else
			{
				s_State = 1;
				ErrCount = 0;
				s_return = NB_OVERTIME;
			}
		break;
			
		case 4:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= WaitTime) 
			{
				s_State = 1;
				ErrCount = 0;
				s_return = NB_OK;
			}
		break;
	}
	
	return s_return;
}

void NBIOT_Process(void)	//NB进程1 接收进程
{
	static u16 s_PackCount = 0, s_PackStartAddr[10], s_PackEndAddr[10];
	unsigned char i = 0, PackStartCount = 0, PackEndCount = 0;
	u16  nTemp, RecPackNum = 0, PackLength = 0, RecLength = 0;
	char buf[5], *p1;
	static TC_COMM_MSG s_RxFrame;
	unsigned char PackBuff[MAX_NB_BUF], DataPack[50];
		
	if(g_Uart3RxFlag == TRUE)
	{
		g_Uart3RxFlag = FALSE;

		u1_printf("%s", g_USART3_RX_BUF);			
		
		if(GetComTestFlag() == FALSE)
		{
			NBIOT_Process2();
		}
		
		if((strstr((const char *)g_USART3_RX_BUF,"+CPIN: READY")) != NULL )
		{
			u1_printf(" M5311 Ready\r\n");
			g_TCRecStatus.LinkStatus = NotConnected;
		}
		else if((p1 = strstr((const char *)g_USART3_RX_BUF,"+CEREG:")) != NULL )
		{
//			nTemp = atoi( (const char *) (p1+10));
//			if(nTemp == 1 || nTemp == 5)
//			{
//				if(g_TCRecStatus.LinkStatus == NotConnected)
//				{
//					g_TCRecStatus.LinkStatus = Attach;
//				}
//			}
//			else
//			{
//				g_TCRecStatus.LinkStatus = NotConnected;
//			}
		}
		else if((p1 = strstr((const char *)g_USART3_RX_BUF,"+CSQ:")) != NULL )
		{
			strncpy(buf,p1+6,2);
			s_Signal = atoi( buf);
		}
		else if((strstr((const char *)g_USART3_RX_BUF,"+IP:")) != NULL )
		{
			u1_printf(" NB 获取IP成功\r\n");
			g_TCRecStatus.LinkStatus = GotIP;
			delay_ms(100);
		}
		else if((strstr((const char *)g_USART3_RX_BUF,"CONNECT FAIL")) != NULL )
		{
			u1_printf(" NB 连接失败\r\n");
			g_TCRecStatus.LinkStatus = NotConnected;
		}
		else if((strstr((const char *)g_USART3_RX_BUF,"+IPCLOSE:")) != NULL )
		{
			u1_printf(" NB TCP连接被关闭\r\n");
			g_TCRecStatus.LinkStatus = NotConnected;
		}
		else if((strstr((const char *)g_USART3_RX_BUF,"CONNECT OK")) != NULL )
		{
			u1_printf(" NB TCP连接成功\r\n");
			g_TCRecStatus.LinkStatus = TCPConnected;
			SetTCProtocolRunFlag(TRUE);
		}
		else if((strstr((const char *)g_USART3_RX_BUF,"CONNECTED")) != NULL )
		{
			u1_printf(" NB TCP已连接\r\n");
			g_TCRecStatus.LinkStatus = TCPConnected;
			SetTCProtocolRunFlag(TRUE);
		}
		else if((strstr((const char *)g_USART3_RX_BUF,"CONNECTING")) != NULL )
		{
			u1_printf(" NB TCP正在连接\r\n");
			g_TCRecStatus.LinkStatus = Connecting;
		}
		else if((strstr((const char *)g_USART3_RX_BUF,"+IPSTATUS: 0,")) != NULL )
		{
			u1_printf(" NB TCP连接已关闭\r\n");
			g_TCRecStatus.LinkStatus = NotConnected;
		}
		else if((strstr((const char *)g_USART3_RX_BUF,"CLOSED")) != NULL )
		{
			u1_printf(" NB TCP连接已关闭\r\n");
			g_TCRecStatus.LinkStatus = NotConnected;
		}
//		else if((strstr((const char *)g_USART3_RX_BUF,"+CME ERROR:")) != NULL )
//		{
////			u1_printf(" NB TCP连接已关闭\r\n");
////			g_TCRecStatus.LinkStatus = NotConnected;
//		}
//		else if((p1 = strstr((const char *)g_USART3_RX_BUF,"+IPRD: 0,")) != NULL )//收到一帧数据包
//		{
//			RecPackNum = atoi( (const char *) (p1+9));
//			p1 = strstr((const char *)g_USART3_RX_BUF,",7E");
//			p1++;

//			if(RecPackNum < MAX_NB_BUF)
//			{
//				for(i=0; i<RecPackNum; i++)
//				{
//					StrToHex(&PackBuff[i], (u8 *)(p1+i*2), RecPackNum);	//将接收到的字符串数据转换为hex
//				}	
//			}
//			else
//			{
//				u1_printf(" NB Data Count Over\r\n");
//				return;
//			}
//			
//			PackStartCount = 0;
//			PackEndCount = 0;
//			
//			for(i=0; i<RecPackNum; i++)
//			{
//				if(PackBuff[i] == 0x7E)	//寻找包头个数
//				{
//					s_PackStartAddr[PackStartCount] = i;
//					PackStartCount++;
//				}
//				else if(PackBuff[i] == 0x21)	//寻找包尾个数
//				{
//					s_PackEndAddr[PackEndCount] = i;
//					PackEndCount++;
//				}
//			}
//			
//			if(PackStartCount == PackEndCount)	//是完整的数据包
//			{
//				s_PackCount = PackStartCount;
//			}
//			else
//			{
//				u1_printf(" PackStartCount != PackEndCount\r\n");
//				return;
//			}
//			
//			if(s_PackCount >= 1 && s_PackCount <= 10)	//最多处理10个数据连包
//			{
//				for(i=0; i<s_PackCount; i++)
//				{
//					memset(NB_RxdBuff, 0 ,sizeof(NB_RxdBuff));
//					UnPackMsg(PackBuff+s_PackStartAddr[i]+1, s_PackEndAddr[i] - s_PackStartAddr[i] - 1, NB_RxdBuff, &PackLength);	//解包
//					
//					if (NB_RxdBuff[(PackLength)-1] == Calc_Checksum(NB_RxdBuff, (PackLength)-1))
//					{
//						hdr = (CLOUD_HDR *)NB_RxdBuff;
//						hdr->protocol = swap_word(hdr->protocol);
//						hdr->device_id = swap_dword(hdr->device_id);
//						hdr->seq_no = swap_word(hdr->seq_no);
//						hdr->payload_len = swap_word(hdr->payload_len);
//						
//						memcpy(DataPack, &NB_RxdBuff[sizeof(CLOUD_HDR)], hdr->payload_len);
//						RecLength = hdr->payload_len + sizeof(CLOUD_HDR) + 1;
//									
//						if (RecLength != (PackLength))
//						{
//							u1_printf(" Pack Length Err\r\n");
//						}
//						else
//						{
//							OnRecTCProtocol(NB_COM, hdr);	//正确的数据包
//						}	
//					}		
//				}				
//			}
//			else
//			{
//				u1_printf(" Packs too much\r\n");
//				return;
//			}
//			
//			g_TCRecStatus.LinkStatus = TCPConnected;

//		}	
		Clear_Uart3Buff(); 	
	}					
}


void NBIOT_Process2(void)	//NB进程2，发送进程
{
	static u8 RunOnce = FALSE;
	u8 err = 0;
	static u16 s_AliveCount = 0, s_TCPCount = 0, s_DataCount, s_WaitingTime, s_WaitCount;
	static u32 s_LastConnectTime, s_LastTCPTime, s_LastTimeData, s_LastTimeAlive, s_OverTime;
	static char str[50];
	SYSTEMCONFIG *p_sys;
	RTC_TimeTypeDef RTC_TimeStructure;
	static unsigned char s_NBStatus = 1, s_LastStatus = 0;
	
	if(RunOnce == FALSE)
	{	
		g_TCRecStatus.LinkStatus = NotConnected;
		
		RunOnce = TRUE;
				
		u1_printf("\r\n Wait for NB Run...\r\n");		
		
		NB_PortInit();
		
		NB_PowerKey();
							
		USART3_Config(NB_BAND_RATE);
		
		u1_printf("\r\n Init NB IoT Finsh\r\n");	

		s_LastTimeData = GetRTCSecond();	
		
		s_LastTimeAlive = GetRTCSecond();	
		
		s_LastConnectTime = GetRTCSecond();
	}
	
	if(g_TCRecStatus.LinkStatus == TCPConnected)
	{
		if(GetLockinTimeFlag())
		{
			ClearLockinTimeFlag();
		}
		else if(DifferenceOfRTCTime(GetRTCSecond(), s_LastConnectTime) >= 1)	
		{
			s_LastConnectTime = GetRTCSecond();
		}
	}
	else
	{
		if(DifferenceOfRTCTime(GetRTCSecond(), s_LastConnectTime) >= 1200)	
		{
			s_LastConnectTime = GetRTCSecond();
			s_NBStatus = 1;
			g_TCRecStatus.LinkStatus = NotConnected;
			u1_printf(" NB 20min未连接，重启模块\r\n");
			SetTCProtocolRunFlag(FALSE);
		}
	}
	
	switch(s_NBStatus)
	{
		case 1:		//复位模组
			err = NB_SendCmd("AT+CMRB\r\n", "REBOOTING", 100, 3,100);
			g_TCRecStatus.LinkStatus = NotConnected;
		SetTCProtocolRunFlag(FALSE);
			if(err == NB_OK)
			{
				s_NBStatus = 3;
			}
			else if(err == NB_MESSAGE)
			{
				s_NBStatus = 3;
			}
			else if(err == NB_OVERTIME)
			{
				s_NBStatus = 3;
				u1_printf(" RESET无响应,重启电源\r\n");
				NB_POWER_OFF();
				delay_ms(1000);
				delay_ms(1000);
				NB_POWER_ON();
				delay_ms(500);
				NB_PowerKey();
			}
		break;
		
		case 2:
			//发送进程进入空闲态，检测状态
			if(g_TCRecStatus.LinkStatus == NotConnected)
			{
				if(s_LastStatus != g_TCRecStatus.LinkStatus)
				{
					s_LastStatus = g_TCRecStatus.LinkStatus;
					s_NBStatus = 15;
				}
				
			}
			else if(g_TCRecStatus.LinkStatus == Connecting)
			{
				if(s_LastStatus != g_TCRecStatus.LinkStatus)
				{
					s_LastStatus = g_TCRecStatus.LinkStatus;
				}
				
				if(CalculateTime(GetSystem10msCount(), s_WaitingTime) >= 600) 
				{
					s_WaitCount++;
					s_WaitingTime = GetSystem10msCount();
					RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
					u1_printf("\r\n [%0.2d:%0.2d:%0.2d]", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
					u3_printf("AT+CSQ\r\n");		
					delay_ms(50);					
					u3_printf("AT*ENGINFO=0\r\n");		
				}
				else if(s_WaitCount >= 15)
				{
					s_NBStatus = 1;
					s_WaitCount = 0;
					s_WaitingTime = GetSystem10msCount();
					u1_printf(" 等待IP超时，重启模块\r\n");
				}
			}
			else if(g_TCRecStatus.LinkStatus == TCPConnected)
			{
				if(s_LastStatus != g_TCRecStatus.LinkStatus)
				{
					s_LastStatus = g_TCRecStatus.LinkStatus;
					
					s_NBStatus = 30;
				}
			}
			else if(g_TCRecStatus.LinkStatus == NotConnected)
			{
				if(CalculateTime(GetSystem10msCount(), s_WaitingTime) >= 600) 
				{
					s_WaitCount++;
					s_WaitingTime = GetSystem10msCount();
					RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
					u1_printf("\r\n [%0.2d:%0.2d:%0.2d]", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);	
					u3_printf("AT+CSQ\r\n");		
					delay_ms(50);					
					u3_printf("AT*ENGINFO=0\r\n");				
					
				}
				else if(s_WaitCount >= 15)
				{
					s_NBStatus = 1;
					s_WaitCount = 0;
					s_WaitingTime = GetSystem10msCount();
					u1_printf(" 等待IP超时，重启模块\r\n");
				}
				
				if(s_LastStatus != g_TCRecStatus.LinkStatus)
				{
					s_LastStatus = g_TCRecStatus.LinkStatus;
					s_NBStatus = 3;
				}
			}
			else if(g_TCRecStatus.LinkStatus == TCPConnected)
			{
				if(s_LastStatus != g_TCRecStatus.LinkStatus)
				{
					s_LastStatus = g_TCRecStatus.LinkStatus;
					s_NBStatus = 15;
					s_WaitCount = 0;
				}
			}
		break;
		
		case 3:		//测试模块AT应答
			err = NB_SendCmd("AT\r\n", "OK", 100, 3, 50);
			if(err == NB_OK)
			{
				s_NBStatus++;
			}
			else if(err == NB_OVERTIME)
			{
				s_NBStatus = 2;
				u1_printf(" AT无响应\r");
			}
		break;
		
		case 4:		//开启或关闭回显
			err = NB_SendCmd("ATE1\r\n", "OK", 100, 3,30);
			if(err == NB_OK)
			{
				s_NBStatus++;
			}
			else if(err == NB_OVERTIME)
			{
				s_NBStatus = 2;
				u1_printf(" ATE无响应\r");
			}
		break;
			
		case 5:		//设置波特率
			err = NB_SendCmd("AT+IPR=115200\r\n", "OK", 100, 3,30);
			if(err == NB_OK)
			{
				s_NBStatus++;
			}
			else if(err == NB_OVERTIME)
			{
				s_NBStatus = 2;
				u1_printf(" WAKETIME无响应\r");
			}
		break;


		case 6:		//设置WAKEUP
			err = NB_SendCmd("AT+CMSYSCTRL=1,0\r\n", "OK", 100, 3,30);
			if(err == NB_OK)
			{
				s_NBStatus++;
			}
			else if(err == NB_OVERTIME)
			{
				s_NBStatus = 2;
				u1_printf(" CMSYSCTRL Err\r\n");
			}
		break;
			
		case 7:		//设置STATUS
			err = NB_SendCmd("AT+CMSYSCTRL=0,0\r\n", "OK", 100, 3,30);
			if(err == NB_OK)
			{
				s_NBStatus++;
			}
			else if(err == NB_OVERTIME)
			{
				s_NBStatus = 2;
				u1_printf(" CMSYSCTRL Err\r\n");
			}
		break;
		
		case 8:		//设置接收数据格式
			err = NB_SendCmd("AT+IPRCFG=1,0,1\r\n", "OK", 100, 3,30);
			if(err == NB_OK)
			{
				s_NBStatus++;
			}
			else if(err == NB_OVERTIME)
			{
				s_NBStatus = 2;
				u1_printf(" 设置接收格式失败\r\n");
			}
		break;
		
		case 9:		//设置PSM
			err = NB_SendCmd("AT+CPSMS=1,,,\"00101111\",\"00001000\"\r\n", "OK", 100, 3,30);//	3214	3324
			if(err == NB_OK)
			{
				s_NBStatus++;
			}
			else if(err == NB_OVERTIME)
			{
				s_NBStatus = 2;
				u1_printf(" 设置接收格式失败\r\n");
			}
		break;
			
		case 10:		//设置eDRX
			err = NB_SendCmd("AT*EDRXCFG=1,5,\"0010\",\"0001\"\r\n", "+CME ERROR", 100, 3,30);	//eDRX   PTW	
//			err = NB_SendCmd("AT*EDRXCFG=0\r\n", "+CME ERROR", 100, 3,30);	//eDRX   PTW	
			if(err == NB_OK)
			{
				s_NBStatus++;
			}
			else if(err == NB_MESSAGE)
			{
				s_NBStatus++;
				u1_printf(" 设置eDRX失败，参数错误\r\n");
			}
			else if(err == NB_OVERTIME)
			{
				s_NBStatus = 2;
				u1_printf(" 设置eDRX失败\r\n");
			}
		break;
			
		case 11:
			err = NB_SendCmd("AT+CEREG?\r\n", "OK", 100, 3,30);
			if(err == NB_OK)
			{
				s_NBStatus++;
			}
			else if(err == NB_OVERTIME)
			{
				s_NBStatus = 2;
				u1_printf(" 设置AT+CEREG失败\r\n");
			}
		break;
			
		case 12:
			err = NB_SendCmd("AT+CSQ\r\n", "OK", 100, 3,30);
			if(err == NB_OK)
			{
				u1_printf("\r\n 等待IP...\r\n");
				s_WaitingTime = GetSystem10msCount();
				s_NBStatus = 2;
			}
			else if(err == NB_OVERTIME)
			{
				s_NBStatus = 2;
				u1_printf(" AT+CSQ失败\r\n");
			}
		break;
			
		case 15:		//检测Attach状态
			err = NB_SendCmd("AT+CEREG=4\r\n", "+CEREG:", 100, 3,30);
			if(err == NB_OK)
			{
				s_NBStatus++;
			}
			else if(err == NB_MESSAGE)
			{
				s_NBStatus++;
			}
			else if(err == NB_OVERTIME)
			{
				s_NBStatus++;
				u1_printf(" CMD OverTime15\r\n");
			}

		break;
		
		case 16:		//检测信号
			err = NB_SendCmd("AT+CSQ\r\n", "+CSQ:", 100, 3,50);
			if(err == NB_OK)
			{
				s_NBStatus++;
			}
			else if(err == NB_MESSAGE)
			{
				s_NBStatus++;
			}
			else if(err == NB_OVERTIME)
			{
				u1_printf(" CMD OverTime16\r\n");
				s_NBStatus++;
			}
		break;
		
		case 17:
			p_sys = GetSystemConfig();	
			u1_printf("[NB-IOT]建立TCP链接\r\n");
			sprintf(str,"AT+IPSTART=0,\"TCP\",\"%d.%d.%d.%d\",%d\r\n",p_sys->Gprs_ServerIP[0],p_sys->Gprs_ServerIP[1],p_sys->Gprs_ServerIP[2],p_sys->Gprs_ServerIP[3],p_sys->Gprs_Port);	
			WakeUpNBModules();
			s_NBStatus++;
		break;
			
		case 18:	//建立TCP链接
			u3_printf("%s", str);
			s_NBStatus++;
			s_LastTCPTime = GetSystem10msCount();
		break;
		
		case 19:		
			if((strstr((const char *)g_USART3_RX_BUF,"OK")) != NULL )
			{
				s_TCPCount = 0;
				s_WaitCount = 0;
				s_WaitingTime = GetSystem10msCount();
				g_TCRecStatus.LinkStatus = Connecting;

				s_NBStatus++;
				s_LastTCPTime = GetSystem10msCount();
			}
			else if((strstr((const char *)g_USART3_RX_BUF,"operation not allowed")) != NULL )
			{
				s_TCPCount = 0;
				s_WaitCount = 0;
				s_WaitingTime = GetSystem10msCount();
				g_TCRecStatus.LinkStatus = Connecting;
				u1_printf(" TCP Connecting\r\n");
				s_NBStatus++;
				s_LastTCPTime = GetSystem10msCount();
			}
			else if((strstr((const char *)g_USART3_RX_BUF,"CONNECT FAIL!")) != NULL )
			{
//				s_TCPCount++;
//				s_NBStatus = 17;
//				s_LastTCPTime = GetSystem10msCount();
			}
			else if(CalculateTime(GetSystem10msCount(), s_LastTCPTime) >= 100) 
			{
				s_LastTCPTime = GetSystem10msCount();
				s_TCPCount++;
				s_NBStatus = 15;
			}
			else if(s_TCPCount >= 3)
			{
				s_TCPCount = 0;
				s_NBStatus = 1;
				g_TCRecStatus.LinkStatus = NotConnected;
				u1_printf(" TCP OverTime\r\n");
				s_LastTCPTime = GetSystem10msCount();
			}
			
		break;
		
		case 20:
			
			//检测信噪比
			err = NB_SendCmd("AT*ENGINFO=0\r\n", "*ENGINFOSC:", 100, 3,50);
			if(err == NB_OK)
			{
				s_NBStatus = 2;
				u1_printf(" 等待连接...\r\n");
			}
			else if(err == NB_MESSAGE)
			{
				s_NBStatus = 2;
				u1_printf(" 等待连接...\r\n");
			}
			else if(err == NB_OVERTIME)
			{
				u1_printf(" CMD OverTime16\r\n");
				s_NBStatus = 2;
			}
		break;
		case 30:	//发送心跳包
			if(g_TCRecStatus.LinkStatus == TCPConnected)
			{				
				if(s_AlivePackFlag == TRUE)
				{
					RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
					u1_printf(" [%0.2d:%0.2d:%0.2d]Alive\r\n", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
					Mod_Send_Alive(NB_COM);
					s_LastTimeAlive = GetRTCSecond();
					s_NBStatus++;
				}
				else
				{
					s_NBStatus = 32;				
				}
			}
			else
			{
				s_NBStatus = 2;
			}
		break;
		
		case 31:	
			if((strstr((const char *)g_USART3_RX_BUF,"+IPSEND:")) != NULL )
			{
				s_NBStatus++;
				s_AliveCount = 0;
				s_LastTimeAlive = GetRTCSecond();
			}
			else if((strstr((const char *)g_USART3_RX_BUF,"OK")) != NULL )
			{
				s_NBStatus++;
				s_AliveCount = 0;
				s_LastTimeAlive = GetRTCSecond();
			}
			else if(DifferenceOfRTCTime(GetRTCSecond(), s_LastTimeAlive) >= 5)	
			{
				s_LastTimeAlive = GetRTCSecond();
				s_AliveCount++;
				s_NBStatus--;
			}
			else if(s_AliveCount >= 2)
			{
				s_NBStatus++;
				s_AliveCount = 0;
				s_LastTimeAlive = GetRTCSecond();
			}
		break;

		case 32:	//发送数据包
			if(g_TCRecStatus.LinkStatus == TCPConnected)
			{				
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
				u1_printf("\r\n [%0.2d:%0.2d:%0.2d]", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
				Mod_Report_Data(NB_COM);
				s_LastTimeData = GetRTCSecond();
				s_NBStatus++;
			}
			else
			{
				s_NBStatus = 2;
			}
		break;
		
		case 33:
			if((strstr((const char *)g_USART3_RX_BUF,"+IPSEND:")) != NULL )
			{
				s_NBStatus++;
				s_LastTimeData = GetRTCSecond();	
				s_OverTime = GetRTCSecond();
				s_DataCount = 0;
			}
			else if((strstr((const char *)g_USART3_RX_BUF,"OK")) != NULL )
			{
				s_NBStatus++;
				s_LastTimeData = GetRTCSecond();	
				s_OverTime = GetRTCSecond();
				s_DataCount = 0;
			}
			else if(DifferenceOfRTCTime(GetRTCSecond(), s_LastTimeData) >= 5)	
			{
				s_LastTimeData = GetRTCSecond();
				s_DataCount++;
				s_NBStatus--;
			}
			else if(s_DataCount >= 2)
			{
				s_NBStatus = 38;
				s_LastTimeData = GetRTCSecond();	
				s_DataCount = 0;
			}
		break;
			
		case 34:
			if(g_TCRecStatus.DataAckFlag == TRUE)
			{		
				s_NBStatus++;	
				s_OverTime = GetRTCSecond();				
			}
			else if(DifferenceOfRTCTime(GetRTCSecond(), s_OverTime) >= 20)	
			{
				u1_printf(" 等待应答包超时\r\n");
				s_OverTime = GetRTCSecond();
				s_NBStatus = 38;
			}
		break;
		
		case 35:
			if(GetServerConfigCmdFlag() == FALSE)
			{
				if(DifferenceOfRTCTime(GetRTCSecond(), s_OverTime) >= 3)
				{
					u1_printf(" 超时未接收0X63\r\n");
					s_NBStatus = 36;
				}
				
			}
			else if(GetServerConfigCmdFlag() == TRUE)
			{
				s_NBStatus = 36;					
			}					
		break;
			
		case 36:
			g_TCRecStatus.DataAckFlag = FALSE;
			SetServerConfigCmdFlag(FALSE);
			s_NBStatus = 37;
			s_LastTimeData = GetRTCSecond();
		break;
		
		case 37:
			p_sys = GetSystemConfig();
		
			RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);				
			if(DifferenceOfRTCTime(GetRTCSecond(), s_LastTimeAlive) >= p_sys->Heart_interval)	
			{
				if(DifferenceOfRTCTime(GetRTCSecond(), s_LastTimeData) >= p_sys->Data_interval)	
				{				
					s_OverTime = GetRTCSecond();
					u1_printf(" Time To Alive\r\n");
					s_LastTimeAlive = GetRTCSecond();
					s_AlivePackFlag = TRUE;
				}
			}
			
			if(g_TCRecStatus.AliveAckFlag == TRUE)
			{
				g_TCRecStatus.AliveAckFlag = FALSE;
				s_LastTimeAlive = GetRTCSecond();
				s_AlivePackFlag = FALSE;
			}
				
			if(DifferenceOfRTCTime(GetRTCSecond(), s_LastTimeData) >= p_sys->Data_interval)	
			{
				s_LastTimeData = GetRTCSecond();	
				s_OverTime = GetRTCSecond();
				s_STM32StopFlag = FALSE;
				if(g_TCRecStatus.LinkStatus == NotConnected)
				{	
					s_NBStatus = 15;
					WakeUpNBModules();
				}
				else
				{
					s_NBStatus = 2;
				}
			}
			else
			{
				if(s_STM32StopFlag)
				{
					SetStopModeTime(TRUE, STOP_TIME);
				}
			}		
		break;
					
		case 38:	//关闭TCP链接
			err = NB_SendCmd("AT+IPCLOSE=0\r\n", "+CME ERROR", 100, 3, 50);
			if(err == NB_OK)
			{
				s_NBStatus++;
				g_TCRecStatus.LinkStatus = NotConnected;
			}
			else if(err == NB_MESSAGE)
			{
				u1_printf(" TCP已关闭.\r\n");
				s_NBStatus++;		
				g_TCRecStatus.LinkStatus = NotConnected;
				
			}
			else if(err == NB_OVERTIME)
			{
				u1_printf(" CMD OverTime35\r\n");
				s_NBStatus++;
				g_TCRecStatus.LinkStatus = NotConnected;
				
			}
		break;
		
		case 39:
			err = NB_SendCmd("AT*ENTERSLEEP\r\n", "OK", 100, 3, 50);
			if(err == NB_OK)
			{
				s_NBStatus = 37;
				s_STM32StopFlag = TRUE;
			}
			else if(err == NB_OVERTIME)
			{
				u1_printf(" CMD OverTime36\r\n");
				s_NBStatus = 37;
				s_STM32StopFlag = TRUE;
				
			}	
		break;
		
	}
}










#include "main.h"
#include "SIM.h"

#define COM_DATA_SIZE	256

#define APN_CMNET 	1		//中国移动

#define APN_UNINET 	2		//中国联通

#define		WAITINGTIME		200		//默认等待上电时间		2S
#define		INTERVALTIME	30		//等待间隔		

static u8 s_SIMConnectedFlag = FALSE;	//SIM连接成功标志

void SIM800CPortInit(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA , ENABLE);	 

	GPIO_InitStructure.GPIO_Pin = SIM800C_ENABLE_PIN;	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_Init(SIM800C_ENABLE_TYPE, &GPIO_InitStructure);	
}


void SIM800CProcess(unsigned short nMain10ms)
{
	u8 i;
	u16 nTemp = 0;
	static u16 s_LastTime = 0, s_OverCount = 0, s_WaitingTime = 600;
	static unsigned int s_RTCSumSecond = 0, s_AliveRTCCount = 0xfffe0000;
	static u8 s_APN = 0, s_APNFlag = 0, s_State = 1, s_Step = 1, RunOnce = FALSE, s_ErrCount = 0, s_RetryCount = 0;
	static u8 s_ATAckCount = 0, s_ReportConfigFlag = FALSE;
	char * p1 = NULL, buf[10], str[100];
	SYSTEMCONFIG *p_sys;
	RTC_TimeTypeDef RTC_TimeStructure;
	RTC_DateTypeDef RTC_DateStruct;
	static u8 s_LastRTCDate = 32;
	static u8 s_StartCalTimeFlag = TRUE;	//开始计算上报时间标志
	static u16 s_StartTime = 0, s_EndTime = 0;
	
	p_sys = GetSystemConfig();
	
	if(RunOnce == FALSE)
	{
		RunOnce = TRUE;
		s_LastTime = GetSystem10msCount();
		g_TCRecStatus.LastLinkTime = GetRTCSecond();
		RTC_GetDate(RTC_Format_BIN, &RTC_DateStruct);
		s_LastRTCDate = RTC_DateStruct.RTC_Date;
	}
			
	if( DifferenceOfRTCTime(GetRTCSecond(),g_TCRecStatus.LastLinkTime) >= 1800 )
	{
		if(GetUpdataTimeFlag())
		{
			SetUpdataTimeFlag(FALSE);
			g_TCRecStatus.LastLinkTime = GetRTCSecond();
		}
		else
		{
			u1_printf("\r\n Reboot the Device\r\n");
			while (DMA_GetCurrDataCounter(DMA1_Channel4));
			Sys_Soft_Reset();
		}
	}
	

			
	switch(s_State)
	{	
		case 0:
			s_RetryCount++;
			u1_printf("\r\n Retry count:%d\r\n", s_RetryCount);
			s_State++;
			p_sys = GetSystemConfig();
			SetLogErrCode(LOG_CODE_RETRY);
			StoreOperationalData();
			if(s_RetryCount >= p_sys->Retry_Times)
			{
				SetLogErrCode(LOG_CODE_TIMEOUT);
				s_RTCSumSecond = GetRTCSecond();	
				s_LastTime = GetSystem10msCount();
				u1_printf(" Over time Can't connect the GPRS\r\n");
				s_RetryCount = 0;
				s_State = 53;
				break;
			}
		break;
		
		case 1:   //模块断电
			SetTCProtocolRunFlag(FALSE);
			SetTCModuleReadyFlag(FALSE);	
			SIM800CPortInit();
			SIM_PWR_OFF();
			USART2_Config(SIM_BAND_RATE);
			s_LastTime = GetSystem10msCount();
			s_State++;
		
			p_sys = GetSystemConfig();
			
			if(p_sys->LowpowerFlag == 1)	//长链接设备
			{
				s_WaitingTime = WAITINGTIME;
				u1_printf("\r\n [SIM]Power off,delay 2s.\r\n");
			}
			else 
			{
				if((p_sys->State_interval <= INTERVALTIME) || s_RetryCount)
				{
					s_WaitingTime = WAITINGTIME;
					u1_printf("\r\n [SIM]Power off,delay 2s.\r\n");
				}
				else
				{
					s_WaitingTime = ((p_sys->State_interval) - INTERVALTIME)*100 + WAITINGTIME;
					u1_printf("\r\n [SIM]Power off,delay %ds.\r\n", s_WaitingTime/100);
				}
			}
			
		break;
		 
		case 2:	//断电后延时5S上电，模块上电自动开机
			if( CalculateTime(GetSystem10msCount(),s_LastTime) >= s_WaitingTime )
			{
				u1_printf("\r\n [SIM]Power on,delay 16s.\r\n");
				SIM_PWR_ON();
				s_LastTime = GetSystem10msCount();
				s_State++;
				
				if(s_StartCalTimeFlag)
				{
					s_StartCalTimeFlag = FALSE;
					s_StartTime = GetSystem10msCount();
				}
			}
		break;
			
		case 3: 	//上电完成后，初始化端口
			if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 1600 )
			{
				USART2_Config(SIM_BAND_RATE);
				u1_printf("\r\n [SIM]Check baudrate\r\n");
				u2_printf("AT\r");
				delay_ms(50);
				u2_printf("AT\r");
				s_LastTime = GetSystem10msCount();
				s_State++;
			}
		break;
			
		case 4:  	//检测模块的波特率
			if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 6 )
			{
				Clear_Uart2Buff();
				u2_printf("AT+CIURC=0\r");	//发送"AT\r"，查询模块是否连接，返回"OK"
				u1_printf("[SIM]AT+CIURC=0\r\n");
				s_LastTime = GetSystem10msCount();
				s_State++;
			}
		break;
		
		case 5:	//延时100MS,检测响应数据
			if(g_Uart2RxFlag == TRUE)
			{				
				if(strstr((char *)g_USART2_RX_BUF, "OK"))
				{
					s_ErrCount = 0;
					s_State++;	
					u1_printf("Close Call Ready\r\n");
				}			
				Clear_Uart2Buff();
				
			}
			else if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 20 )
			{				
					u1_printf("[SIM]OK No Ack\r\n");
					s_State--;   //回上一个状态
					s_ErrCount++;   //重试次数加1
					if(s_ErrCount >= 10 )
					{
						SetLogErrCode(LOG_CODE_NOMODULE);
						s_ErrCount = 0;
						s_State = 0;	//模块重新上电初始化
					}						
			}
	 	break;
		
		case 6:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 20) 
			{
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			

				switch(s_Step)
				{
					case 1:
						u2_printf("ATE0\r");
					break;
					case 2:
						u2_printf("AT+CPIN?\r");
					break;
					case 3:
						u2_printf("AT+CCID\r");
					break;
					case 4:
						u2_printf("AT+CIMI\r");
					break;
					case 5:
						u2_printf("AT+CGSN\r");
					break;
					case 6:
						u2_printf("AT+CSQ\r");	
					break;
					case 7:
						u2_printf("AT+COPS?\r");
					break;	
					case 8:
						u2_printf("AT+CGATT=1\r");		
					break;				
					case 9:
						u2_printf("AT+CGREG?\r");
					break;		
					case 10:
						u2_printf("AT+CREG?\r");
					break;					
					case 11:
						u2_printf("AT+CGATT?\r");
					break;				
					case 12:
						u2_printf("AT+CGATT=1\r");
					break;
				}
				s_LastTime = GetSystem10msCount();
				s_State++;
			}		
		break;
		
		case 7:

			if(g_Uart2RxFlag == TRUE)
			{
				u1_printf("%s\r\n", g_USART2_RX_BUF);
											
				switch(s_Step)
				{
					case 1:
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
							s_LastTime = GetSystem10msCount();							
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
					break;
						
					case 2:
					//	u1_printf("检测SIM卡： ");
						if((strstr((const char *)g_USART2_RX_BUF,"+CPIN: READY")) != NULL )
						{					
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
							s_LastTime = GetSystem10msCount();		
							u1_printf("\r\n  SIM Card OK\r\n");
						}
						else
						{						
							s_State--;	
							s_ErrCount++;
							
							if(s_ErrCount >= 5)
							{
								SetLogErrCode(LOG_CODE_NO_CARD);
								u1_printf(" No Card!\r\n");
								s_ErrCount = 0;
								s_State = 1;
								s_Step = 1;
								
							}
						}						
					break;	
						
					case 3:				//ICCID查询
						//中国移动:898600；898602；898604；898607 
						//中国联通:898601、898606、898609
						//中国电信:898603、898611。
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{
							if((g_USART2_RX_BUF[2]=='8')&&(g_USART2_RX_BUF[3]=='9')&&(g_USART2_RX_BUF[4]=='8')&&(g_USART2_RX_BUF[5]=='6'))
							{
								i = (g_USART2_RX_BUF[6] - '0') * 10 + (g_USART2_RX_BUF[7] - '0');
								switch(i)
								{
									//移动卡
									case 0:
									case 2:
									case 4:
									case 7:
										s_APNFlag = 1;	
										s_APN = APN_CMNET;
//										u1_printf("CCID: APN SET TO CMNET\r\n");
										break;
									//联通卡
									case 1:
									case 6:
									case 9:
										s_APNFlag = 1;	
										s_APN = APN_UNINET;
//										u1_printf("CCID: APN SET TO UNINET\r\n");
										break;										
									default:
										s_APNFlag = 0;
//										u1_printf("CCID: NO APN INFO\r\n");	
										SetLogErrCode(LOG_CODE_UNDEFINE);
										break;
								}
							}
										
						//	u1_printf("制造商：%s\r\n", g_USART2_RX_BUF);
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
							s_LastTime = GetSystem10msCount();							
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
					break;
						
					case 4:			//IMSI查询
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{
							if(!s_APNFlag &&(g_USART2_RX_BUF[2]=='4')&&(g_USART2_RX_BUF[3]=='6')&&(g_USART2_RX_BUF[4]=='0'))
							{
								i = (g_USART2_RX_BUF[5] - '0') * 10 + (g_USART2_RX_BUF[6] - '0');
								switch(i)
								{
									//移动卡
									case 0:
									case 2:
									case 4:
									case 7:
										s_APNFlag = 1;	
										s_APN = APN_CMNET;
//										u1_printf("IMSI: APN SET TO CMNET\r\n");
										break;
									//联通卡
									case 1:
									case 6:
									case 9:
										s_APNFlag = 1;	
										s_APN = APN_UNINET;
//										u1_printf("IMSI: APN SET TO UNINET\r\n");
										break;										
									default:
										s_APNFlag = 0;	
//										u1_printf("IMSI: NO APN INFO\r\n");	
										SetLogErrCode(LOG_CODE_UNDEFINE);
										break;
								}
							}
								
						//	u1_printf("模块名称：%s\r\n", g_USART2_RX_BUF);
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
							s_LastTime = GetSystem10msCount();							
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
					break;
						
					case 5:
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{
						//	u1_printf("序列号：%s\r\n", g_USART2_RX_BUF);
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
							s_LastTime = GetSystem10msCount();							
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
					break;
						
					case 6:	
					//	u1_printf("检测信号质量： ");
						if((p1 = strstr((const char *)g_USART2_RX_BUF,"+CSQ:")) != NULL )
						{							
							//u1_printf("%s\r\n", g_USART2_RX_BUF);							
							strncpy(buf,p1+6,2);
							nTemp = atoi( buf);
							SetCSQValue(nTemp);
							if( nTemp < 10 )								
							{
								s_State--;	
								s_ErrCount++;
								SetLogErrCode(LOG_CODE_WEAKSIGNAL);
								u1_printf(" Weak Signal\r\n");
							}
							else
							{
								s_State--;	
								s_Step++;	
								s_ErrCount = 0;
								ClearLogErrCode(LOG_CODE_WEAKSIGNAL);
								u1_printf("\r\n Signal OK\r\n");
							}
						}
						else
						{
//							u1_printf("    Error\r\n");
							s_State--;	
							s_ErrCount++;
						}		
					break;		
						
					case 7:
						//u1_printf(" 运营商名称： ");
						if((p1 = strstr((const char *)g_USART2_RX_BUF,"+COPS:")) != NULL )
						{				
							if(!s_APNFlag)
							{
								if((strstr((const char *)g_USART2_RX_BUF,"CHINA MOBILE")) != NULL )  
								{
									s_APN = APN_CMNET;
									s_APNFlag = 1;
									u1_printf("COPS: CHINA MOBILE\r\n");
									s_Step++;	
									s_State--;		
									s_ErrCount = 0;
								}
								else if((strstr((const char *)g_USART2_RX_BUF,"UNICOM")) != NULL )  
								{
									s_APN = APN_UNINET;
									s_APNFlag = 1;
									u1_printf("COPS: UNICOM\r\n");
									
									s_Step++;	
									s_State--;		
									s_ErrCount = 0;
								}
								else
								{
									u1_printf("COPS: UNRECOGNIZABLED NETWORK\r\n");
									SetLogErrCode(LOG_CODE_UNDEFINE);
									s_State--;		
									s_ErrCount ++;
								}
							}
							else
							{
								s_Step++;	
								s_State--;		
								s_ErrCount = 0;
							}
	
					
							s_LastTime = GetSystem10msCount();		
						//	u1_printf("%s\r\n", g_USART2_RX_BUF);
						}
						else
						{
							u1_printf("    Error\r\n");
							s_State--;	
							s_ErrCount++;
						}
					break;	
						
					case 8:
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{
							u1_printf(" Set GPRS Attach OK\r\n");
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
							s_LastTime = GetSystem10msCount();	
							ClearLogErrCode(LOG_CODE_UNATTACH);
						}
						else
						{
							s_State--;	
							s_ErrCount++;
							if(s_ErrCount >= 100)
							{		
								u1_printf(" GPRS Attach Fail,Check SIM Card\r\n");
								s_ErrCount = 0;								
								s_Step++;
							}
						}
						
					break;
										
					case 9:
						//u1_printf(" 模块注册网络： ");
						if((p1 = strstr((const char *)g_USART2_RX_BUF,"+CGREG:")) != NULL )
						{						
							//u1_printf("%s\r\n", g_USART2_RX_BUF);
							
							nTemp = atoi( (const char *) (p1+10));
							switch( nTemp )								
							{
								case 0:
				//				u1_printf("[SIM]模块注册--未注册\r\n");
								s_State--;
								s_ErrCount++;
								break;
								
								case 1:
								u1_printf("[SIM]Registered\r\n");
								s_State--;
								s_Step++;
								s_Step++;		//调过GSM查询
								s_ErrCount = 0;
								ClearLogErrCode(LOG_CODE_UNATTACH);
								break;
								
								case 2:
				//				u1_printf("[SIM]模块注册--正在注册，查询次数=%d\r\n",s_ErrCount);
								s_State--;
								s_ErrCount++;

								break;
								
								case 3:
				//				u1_printf("\r\n [SIM]模块注册--注册被拒, 次数=%d\r\n",s_ErrCount);
								s_State--;
								s_ErrCount++;
								break;
								
								default:
				//				u1_printf("[SIM]模块注册--注册状态未识别\r\n");
								s_State--;
								s_ErrCount++;
								break;	
							}
							
							if(s_ErrCount == 100)
							{		
								u1_printf(" GPRS Registrat Fail\r\n");
								s_ErrCount = 0;								
								s_Step++;
								SetLogErrCode(LOG_CODE_UNATTACH);
							}
							
						}
						else
						{
//							u1_printf("    Error\r\n");
							s_State--;	
							s_ErrCount++;
						}	
							
					break;
						
					case 10:
						//u1_printf(" 模块注册网络： ");
						if((p1 = strstr((const char *)g_USART2_RX_BUF,"+CREG:")) != NULL )
						{						
							//u1_printf("%s\r\n", g_USART2_RX_BUF);
							
							nTemp = atoi( (const char *) (p1+9));
							switch( nTemp )								
							{
								case 0:
						//		u1_printf("[SIM]模块注册--未注册\r\n");
								s_State--;
								s_ErrCount++;
								break;
								
								case 1:
								u1_printf("[SIM]Registered\r\n");
								s_State--;
								s_Step++;
								s_ErrCount = 0;
								ClearLogErrCode(LOG_CODE_UNATTACH);
								break;
								
								case 2:
					//			u1_printf("[SIM]模块注册--正在注册，查询次数=%d\r\n",s_ErrCount);
								s_State--;
								s_ErrCount++;

								break;
								
								case 3:
					//			u1_printf("\r\n [SIM]模块注册--注册被拒, 次数=%d\r\n",s_ErrCount);
								s_State--;
								s_ErrCount++;
								break;
								
								default:
					//			u1_printf("[SIM]模块注册--注册状态未识别\r\n");
								s_State--;
								s_ErrCount++;
								break;	
							}
							
							if(s_ErrCount == 100)
							{		
					//			u1_printf(" GSM注册网络失败，查询附着状态\r\n");
								SetLogErrCode(LOG_CODE_UNATTACH);
								s_ErrCount = 0;								
								s_Step++;
							}
						}
						else
						{
//							u1_printf("    Error\r\n");
							s_State--;	
							s_ErrCount++;
						}	
							
					break;
					
					case 11:	
						//u1_printf("GPRS附着状态： ");
						if((p1 = strstr((const char *)g_USART2_RX_BUF,"+CGATT:")) != NULL )
						{					
							//u1_printf("%s\r\n", g_USART2_RX_BUF);
							
							nTemp = atoi( (const char *) (p1+8));
							if( nTemp == 0 )								
							{
								s_State--;	
								s_ErrCount++;
								if(s_ErrCount == 100)
								{									
									s_Step++;
								}
							}
							else
							{
//								if(GetGPSRunFlag() && GetGPSDataFlag() == FALSE)
//								{
//									s_State++;	
//								}
//								else
//								{
//									s_State = 10;
//								}
								s_State = 10;
								s_ATAckCount = 0;
								s_Step = 1;	
								s_ErrCount = 0;
							}
						}
						else
						{
							u1_printf("    Error\r\n");
							s_State--;	
							s_ErrCount++;
						}		
					break;		
						
					case 12:
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{
							s_Step--;	
							s_State--;		
							s_ErrCount = 0;
							s_LastTime = GetSystem10msCount();	
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
						
					break;
						
				}
				
				if(s_ErrCount > 200 )
				{
					SetLogErrCode(LOG_CODE_UNATTACH);
					StoreOperationalData();
					s_ErrCount = 0;
					s_State = 0;
					s_Step = 1;
				}
				Clear_Uart2Buff();
				s_LastTime = GetSystem10msCount();
			}
			else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 500) 
			{
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			
	
				s_ATAckCount++;
				
				if(s_ATAckCount >= 5)
				{
					s_ATAckCount = 0;
					u1_printf(" AT No Ack\r\n");
					s_State = 0;
					s_Step = 1;
				}
				else
				{
					s_State--;
					u1_printf(" AT Over Time\r\n");
				}
				s_LastTime = GetSystem10msCount();
				s_ErrCount++;
			}			
		break;
	
//	case 8:
//			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 15) 
//			{
//				switch(s_Step)
//				{
//					case 1:
//						u2_printf("AT+SAPBR=3,1,\"CONTYPE\",\"GPRS\"\r");
//		//				u1_printf("[SIM]->设置网络参数\r\n");
//					break;
//					case 2:
//						u2_printf("AT+SAPBR=3,1,\"APN\",\"CMNET\"\r");
//		//				u1_printf("[SIM]->设置APN\r\n");
//					break;
//					case 3:
//						u2_printf("AT+SAPBR=1,1\r");
//		//				u1_printf("[SIM]->激活移动场景\r\n");
//					break;
//					case 4:
//						u2_printf("AT+SAPBR=2,1\r");
//		//				u1_printf("[SIM]->获取IP\r\n");
//					break;
//					case 5:
//						u2_printf("AT+CLBSCFG=0,3\r");
//		//				u1_printf("[SIM]->查询地址信息\r\n");
//					break;
//					case 6:
//						u2_printf("AT+CLBS=1,1\r");
//			//			u1_printf("[SIM]->获取定位\r\n");
//					break;
//					case 7:
//						u2_printf("AT+SAPBR=0,1\r");
//			//			u1_printf("[SIM]->关闭移动场景\r\n\r\n");
//					break;
//					
//					case 8:
//					break;
//					
//					default:
//						s_State = 1;
//						s_Step = 1;
//					break;
//				}
//				s_LastTime = GetSystem10msCount();
//				s_State++;
//			}
//		
//		break;
//			
//		case 9:
//			if(g_Uart2RxFlag == TRUE)
//			{
//				u1_printf("%s\r\n", g_USART2_RX_BUF);
//								
//				switch(s_Step)
//				{
//					case 1:
//						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
//						{
//							s_Step++;	
//							s_State--;		
//							s_ErrCount = 0;
//													
//						}
//						else if((strstr((const char *)g_USART2_RX_BUF,"ERROR")) != NULL )
//						{
//							s_Step++;	
//							s_State--;		
//							s_ErrCount = 0;
//							
//						}
//						else
//						{
//							s_State--;	
//							s_ErrCount++;
//						}
//					break;
//						
//					case 2:
//						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
//						{
//							s_Step++;	
//							s_State--;		
//							s_ErrCount = 0;
//													
//						}
//						else
//						{
//							s_State--;	
//							s_ErrCount++;
//						}
//					break;
//						
//					case 3:
//						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL)
//						{
//							s_Step++;	
//							s_State--;		
//							s_ErrCount = 0;
//												
//						}
//						else
//						{
//							s_State--;	
//							s_ErrCount++;
//						}
//					break;
//						
//					case 4:
//						if((strstr((const char *)g_USART2_RX_BUF,".")) != NULL )
//						{
//							s_Step++;	
//							s_State--;		
//							s_ErrCount = 0;
//							
//						}
//						else
//						{
//							s_State--;	
//							s_ErrCount++;
//						}
//					break;
//					
//					case 5:
//						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL)
//						{
//							s_Step++;	
//							s_State--;		
//							s_ErrCount = 0;
//												
//						}
//						else
//						{
//							s_State--;	
//							s_ErrCount++;
//						}
//					break;
//						
//					case 6:
//						if((strstr((const char *)g_USART2_RX_BUF,".")) != NULL )
//						{
//							s_Step++;	
//							s_State--;		
//							s_ErrCount = 0;
//							memcpy(str, g_USART2_RX_BUF, g_USART2_RX_CNT);
//							p_str = strtok(str, ",");
//							p_str = strtok(NULL, ",");  
//							SetLongitude(atof(p_str));
//							p_str = strtok(NULL, ",");  
//							SetLatitude(atof(p_str));

//							s_LocationFailCount = 0;
//							u1_printf("\r\n Longitude:%f  Latitude:%f \r\n", GetLongitude(), GetLatitude());
//						}
//						else
//						{
//							s_State--;	
//							s_ErrCount++;					
//						}
//						
//					break;
//						
//					case 7:
//						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
//						{					
//							s_Step = 1;	
//							s_State++;		
//							s_ATAckCount = 0;
//							s_ErrCount = 0;
//						}
//						else
//						{
//							s_State--;	
//							s_ErrCount++;
//						}						
//					break;
//					
//					default:

//					break;
//						
//				}
//				
//				if(s_ErrCount > 20 )
//				{
//					s_ErrCount = 0;
//					s_State = 1;
//					s_Step = 1;
//				}
//				s_LastTime = GetSystem10msCount();	
//				
//				Clear_Uart2Buff();

//			}
//			else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 800) 
//			{
//				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
//				u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			
//	
//				s_ATAckCount++;
//				
//				if(s_ATAckCount >= 2)
//				{
//					s_ATAckCount = 0;
//					u1_printf(" AT No Ack\r\n");
//					s_State++;
//					s_Step = 1;
//				}
//				else
//				{
//					s_State--;
//					u1_printf(" AT Over Time\r\n");
//					s_LocationFailCount++;
//					if(s_LocationFailCount >= 3)
//					{
//						s_LocationFailCount = 0;
//						SetGPSRunFlag(FALSE);
//					}
//				}
//				s_LastTime = GetSystem10msCount();
//				s_ErrCount++;
//			}	
//		break;
			
		case 10:
			if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 20) 
			{
				switch(s_Step)
				{
					case 1:
						u2_printf("AT+CIPCLOSE=1\r");
					break;
					case 2:
						u2_printf("AT+CIPSHUT\r");
					break;
					case 3:
						u2_printf("AT+CIPMODE=1\r");
					break;
					case 4:
						if(s_APNFlag)
						{
							switch(s_APN)
							{
								case APN_CMNET:
									u2_printf("AT+CSTT=\"CMNET\"\r");

									break;
								case APN_UNINET:
									u2_printf("AT+CSTT=\"UNINET\"\r");
									break;
								default:
									u2_printf("AT+CSTT?\r");
									break;
							}
						}	
						else 
						{								
							u2_printf("AT+CSTT?\r");
						}
					break;
					case 5:
						u2_printf("AT+CIICR\r");
					break;
					case 6:
						u2_printf("AT+CIFSR\r");
					break;
					
					case 7:
						u2_printf("AT+CSCLK=2\r");			
					break;
					
					default:
						
						s_State = 1;
						s_Step = 1;
					break;
				}
				s_LastTime = GetSystem10msCount();
				s_State++;
			}
		
		break;
	
		case 11:
			if(g_Uart2RxFlag == TRUE)
			{
				u1_printf("%s\r\n", g_USART2_RX_BUF);
											
				switch(s_Step)
				{
					case 1:
						if((strstr((const char *)g_USART2_RX_BUF,"+CIPCLOSE")) != NULL )
						{
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
													
						}
						else if((strstr((const char *)g_USART2_RX_BUF,"ERROR")) != NULL )
						{
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
							
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
					break;
						
					case 2:
						if((strstr((const char *)g_USART2_RX_BUF,"SHUT OK")) != NULL )
						{
						//	u1_printf("关闭移动场景\r\n");
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;													
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
					break;
						
					case 3:
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{
						//	u1_printf("设置为透传模式\r\n");
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
												
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
					break;
						
					case 4:
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{
						//	u1_printf("APN OK\r\n");
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
													
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
					break;
					
					case 5:
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{
						//	u1_printf("激活移动场景 OK\r\n");
							s_Step++;	
							s_State--;		
							s_ErrCount = 0;
												
						}
						else if((strstr((const char *)g_USART2_RX_BUF,"+PDP: DEACT")) != NULL )
						{
							s_State--;	
							s_Step = 1;
							s_ErrCount++;
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
						
					break;
						
					case 6:
						//u1_printf("模块本地IP：");
						if((strstr((const char *)g_USART2_RX_BUF,".")) != NULL )
						{					
							s_Step = 1;	
							s_State++;		
							s_ErrCount = 0;
							s_ATAckCount = 0;
							
//							s_Step++;	
//							s_State--;		
//							s_ErrCount = 0;
							//u1_printf("%s\r\n", g_USART2_RX_BUF);
						}
						else
						{
							u1_printf("    Error\r\n");
							s_State--;	
							s_ErrCount++;
						}						
					break;
					
					case 7:	//不适用
						if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
						{
							s_Step = 1;	
							s_State++;		
							s_ErrCount = 0;
							s_ATAckCount = 0;
													
						}
						else
						{
							s_State--;	
							s_ErrCount++;
						}
					break;
						
				}
				
				if(s_ErrCount > 50 )
				{
					s_ErrCount = 0;
					s_State = 0;
					s_Step = 1;
				}
				s_LastTime = GetSystem10msCount();	
				
				Clear_Uart2Buff();

			}
			else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 400) 
			{
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			
	
				s_ATAckCount++;
				
				if(s_ATAckCount >= 4)
				{
					s_ATAckCount = 0;
					u1_printf(" AT No Ack\r\n");
					s_State = 0;
					s_Step = 1;
				}
				else
				{
					s_State--;
					u1_printf(" AT Over Time\r\n");
				}
				s_LastTime = GetSystem10msCount();
				s_ErrCount++;
			}	
		break;		
		
		case 12:
			u1_printf("[SIM]Set TCP\r\n");
			sprintf(str,"AT+CIPSTART=\"TCP\",\"%d.%d.%d.%d\",%d\r"
			,p_sys->Gprs_ServerIP[0]
			,p_sys->Gprs_ServerIP[1]
			,p_sys->Gprs_ServerIP[2]
			,p_sys->Gprs_ServerIP[3]
			,p_sys->Gprs_Port	);

			u2_printf(str);	
			u1_printf(str);	//串口输出AT命令
			u1_printf("\r\n");
			s_LastTime = GetSystem10msCount();
			s_State++;		
		break;
		
		case 13:
			if(g_Uart2RxFlag == TRUE)
			{
				u1_printf("%s\r\n", g_USART2_RX_BUF);
													
				if((strstr((const char *)g_USART2_RX_BUF,"ALREADY CONNECT")) != NULL )
				{
					s_Step = 1;	
					s_State = 10;		
					SetTCModuleReadyFlag(FALSE);	
					u1_printf(" GPRS link established\r\n");				
				}
				else if((strstr((const char *)g_USART2_RX_BUF,"CONNECT OK")) != NULL )
				{
					s_Step = 1;	
					s_State++;		
					s_ErrCount = 0;
					SetTCModuleReadyFlag(1);		//连接成功========================
					u1_printf("GPRS CONNECT OK\r\n");	
					ClearLogErrCode(LOG_CODE_UNATTACH);								
				}
				else if((strstr((const char *)g_USART2_RX_BUF,"CONNECT FAIL")) != NULL )
				{		
					s_ErrCount++;
					s_State--;
					u1_printf("GPRS FAIL\r\n");				
				}
				else if((strstr((const char *)g_USART2_RX_BUF,"CONNECT")) != NULL )
				{
					s_ATAckCount = 0;
					s_Step = 1;	
					s_State++;		
					s_ErrCount = 0;
					SetTCModuleReadyFlag(1);		//连接成功========================
					u1_printf("GPRS CONNECT, Data Interval:%ds\r\n", p_sys->Data_interval);
					ClearLogErrCode(LOG_CODE_UNATTACH);					
				}
				else if((strstr((const char *)g_USART2_RX_BUF,"OK")) != NULL )
				{		
					u1_printf("\r\n Wait Connect...\r\n");	
					s_LastTime = GetSystem10msCount();	
				}
				else if((strstr((const char *)g_USART2_RX_BUF,"ERROR")) != NULL )
				{		
					s_ErrCount++;
					s_State--;		
				}
				else
				{
					s_State--;	
					s_ErrCount++;
					u1_printf("Undefine\r\n");
				}
					
				
				if(s_ErrCount > 5 )
				{
					s_ErrCount = 0;
					s_State = 0;
					s_Step = 1;
				}
				s_LastTime = GetSystem10msCount();	
				
				Clear_Uart2Buff();

			}
			else if(CalculateTime(GetSystem10msCount(), s_LastTime) >= 1000) 
			{
				RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);	
				u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);			
	
				s_ATAckCount++;
				
				if(s_ATAckCount >= 2)
				{
					s_ATAckCount = 0;
					SetLogErrCode(LOG_CODE_TIMEOUT);
					u1_printf(" AT No Ack\r\n");
					s_State = 0;
					s_Step = 1;
				}
				else
				{
					s_State--;
					u1_printf(" AT Over time\r\n");
				}
				
				s_LastTime = GetSystem10msCount();
				s_ErrCount++;
			}			
		break;
		
		case 14:
			
			s_State = 50;
			SetTCProtocolRunFlag(TRUE);
			s_LastTime = GetSystem10msCount();
		break;
		
		case 50:
			if(GetTCModuleReadyFlag())		//SIM就绪
			{					
				if(g_PowerDetectFlag && g_485SensorGetFlag)
				{
					if(DifferenceOfRTCTime(GetRTCSecond(), s_AliveRTCCount) >= p_sys->Heart_interval)	//发送心跳包
					{
						s_AliveRTCCount = GetRTCSecond();
						Mod_Send_Alive(SIM_COM);
						delay_ms(200);			
						
						break;
					}
					
					if(s_ReportConfigFlag == FALSE)
					{
						s_ReportConfigFlag = TRUE;
						ReportSystemConfig();
						delay_ms(200);
					}
						
						
					if(g_TCRecStatus.DataAckFlag == FALSE)
					{
						s_EndTime = GetSystem10msCount();
						SetLinkNetTime(CalculateTime(s_EndTime, s_StartTime) );
						Mod_Report_Data(SIM_COM);
					}
					
					s_State++;
					s_LastTime = GetSystem10msCount();
				}
			}
			else
			{
				s_State = 1;
				s_Step = 1;
				s_ErrCount = 0;
				s_LastTime = GetSystem10msCount();
			}
		break;
		
		case 51:
			if(g_TCRecStatus.DataAckFlag)//非长连接设备取消心跳包  2019.2.12
			{
				p_sys = GetSystemConfig();
				SetLogErrCode(LOG_CODE_SUCCESS);
				if(p_sys->LowpowerFlag == 0)
				{
					s_State = 52;	//低功耗设备
					WriteCommunicationSuccessCount();
					SetTCProtocolRunFlag(FALSE);
					s_SIMConnectedFlag = TRUE;
				}
				else if(p_sys->LowpowerFlag == 1)
				{
					s_State = 56;	//非低功耗设备
				}
				s_LastTime = GetSystem10msCount();
				g_TCRecStatus.DataAckFlag = FALSE;
				s_OverCount = 0;
			}
			else if( CalculateTime(GetSystem10msCount(),s_LastTime) >= 1000 )
			{
				s_State--;
				s_LastTime = GetSystem10msCount();
				u1_printf("\r\n Over time can't rec,retry\r\n");
				Clear_Uart2Buff();
				s_OverCount++;
				
				if(p_sys->LowpowerFlag == 0)
				{
					if(s_OverCount >= 3)
					{
						SetLogErrCode(LOG_CODE_NOSERVERACK);
						s_OverCount = 0;
						s_State = 0;
						s_Step = 1;
						s_ErrCount = 0;
					}
				}
			}
			
		break;
					
		case 52:			
			if(GetCmdCloseSocket() == FALSE)
			{
				if(CalculateTime(GetSystem10msCount(),s_LastTime) >= 100)		//延时1s等待GPRS下行数据
				{
					u1_printf("\r\n Wait server over time, sleep\r\n");
					s_State++;
				}
			}
			else if(GetCmdCloseSocket())		//收到关闭Socket指令，可以休眠
			{
				u1_printf("\r\n Rec server ack,sleep\r\n");
				s_State++;
				SetCmdCloseSocket(FALSE);
			}
		break;
		
		case 53:
			s_RTCSumSecond = GetRTCSecond();
			s_State++;
			s_LastTime = GetSystem10msCount();
			StoreOperationalData();
			WriteCommunicationCount();
			s_EndTime = GetSystem10msCount();
			SetLinkNetTime(CalculateTime(s_EndTime, s_StartTime) );
			u1_printf("\r\n 连接耗时:%.2fs\r\n\r\n", CalculateTime(s_EndTime, s_StartTime)/100.0);
		break;
		
		case 54:
			if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSumSecond) >= p_sys->Data_interval)		//达到上报数据时间点
			{
				s_RTCSumSecond = GetRTCSecond();			
				u1_printf("\r\n SIM Start\r\n");
				s_ReportConfigFlag = TRUE;
				s_SIMConnectedFlag = FALSE;
				SetLogErrCode(LOG_CODE_CLEAR);	
				s_RetryCount = 0;
				Clear_Flag();			//清传感器标志，开启传感器检测
				s_State++;
			}		
			else			//未到上报时间，继续休眠
			{		
				if(s_SIMConnectedFlag)
				{
					SetStopModeTime(TRUE, STOP_TIME);	
				}
				else
				{
					SetStopModeTime(TRUE, ERR_STOP_TIME);	
				}
						
			}
		break;
			
		case  55:
			s_State = 1;
			s_Step = 1;
			s_ErrCount = 0;
			s_LastTime = GetSystem10msCount();
		
			RTC_GetDate(RTC_Format_BIN, &RTC_DateStruct);
				
			if(s_LastRTCDate != RTC_DateStruct.RTC_Date)	//日期更新，获取经Latitude
			{
				s_LastRTCDate = RTC_DateStruct.RTC_Date;
				SetGPSRunFlag(TRUE);
				SetGPSPowerFlag(TRUE);
			}
		break;		
			
		case 56:
			s_RTCSumSecond = GetRTCSecond();
			s_State++;
			s_LastTime = nMain10ms;

		break;
		
		case 57:
			if(DifferenceOfRTCTime(GetRTCSecond(), s_RTCSumSecond) >= p_sys->Data_interval)		//达到上报数据时间点
			{
				s_RTCSumSecond = GetRTCSecond();			
				Clear_Flag();			//清传感器标志，开启传感器检测
				s_State = 50;
			}	
			if(DifferenceOfRTCTime(GetRTCSecond(), s_AliveRTCCount) >= p_sys->Heart_interval)	//发送心跳包
			{
				s_AliveRTCCount = GetRTCSecond();
				Mod_Send_Alive(SIM_COM);
			}			
		break;
			
		default:
			s_State = 1;
			u1_printf(" 意外错误!\r\n");
		break;
	}
	
	if(g_Uart2RxFlag == TRUE)
	{
		if(s_State <= 4)
		{
			u1_printf("Sim Rec:%s\r\n", g_USART2_RX_BUF);
			Clear_Uart2Buff();
		}
	}	
	
}
	

#ifndef		_GPS_H_
#define		_GPS_H_

unsigned char GetGPSRunFlag(void);

void SetGPSRunFlag(unsigned char isTrue);

float GetLongitude(void);

void SetLongitude(float temp);

float GetLatitude(void);

void SetLatitude(float temp);


#endif




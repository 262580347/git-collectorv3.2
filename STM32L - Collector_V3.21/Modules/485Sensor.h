#ifndef _485SENSOR_H_
#define _485SENSOR_H_

#include "stm32l1xx_usart.h"
#include "SensorManage.h"

#define  	EXTI_RAINFALL_LINE					EXTI_Line8
#define	 	EXTI_RAINFALL_SOURCE				EXTI_PinSource8
#define		EXTI_RAINFALL_IRQ					EXTI9_5_IRQn
#define		EXTI_RAINFALL_GPIO_PIN				GPIO_Pin_8
#define		EXTI_RAINFALL_GPIO_TYPE				GPIOA
#define		EXTI_RAINFALL_PORTSOURCEGPIO_TYPE	EXTI_PortSourceGPIOA


#define  	EXTI_RAINFALL2_LINE					EXTI_Line15
#define	 	EXTI_RAINFALL2_SOURCE				EXTI_PinSource15
#define		EXTI_RAINFALL2_IRQ					EXTI15_10_IRQn
#define		EXTI_RAINFALL2_GPIO_PIN				GPIO_Pin_15
#define		EXTI_RAINFALL2_GPIO_TYPE			GPIOB
#define		EXTI_RAINFALL2_PORTSOURCEGPIO_TYPE	EXTI_PortSourceGPIOB

#define		SENSOR485_COM	USART3

#define		SENSOR485_ENABLE_GPIO_TYPE		GPIOB
#define 	SENSOR485_ENABLE_GPIO_PIN		GPIO_Pin_1

#define		SENSOR485_ENABLE()		GPIO_SetBits(SENSOR485_ENABLE_GPIO_TYPE, SENSOR485_ENABLE_GPIO_PIN)
#define		SENSOR485_DISABLE()		GPIO_ResetBits(SENSOR485_ENABLE_GPIO_TYPE, SENSOR485_ENABLE_GPIO_PIN)

extern unsigned char g_485SensorGetFlag;
extern unsigned short g_ReportDataWaitingTime;

typedef __packed struct
{
	float	ChannelData[HWL_INPUT_COUNT];
	float	LastChannelData[HWL_INPUT_COUNT];
	unsigned char ErrorCount[20];
	unsigned char SensorOKStatus[20];
}CHANNEL_DATA;

unsigned short Calculate_CRC16(unsigned char *buf,unsigned short length);

void Sensor485_Process(unsigned short nMain10ms);

float GetSensorData(unsigned char SensorNum);
	
unsigned char GetInputChannelCount(void);

void Deinit_485Port(void);

void Init_485Port(void);

void EXTI_Rainfall_Reset(void);

void Uart_Set_485Addr(USART_TypeDef* USARTx, unsigned char sAddr, unsigned char dAddr);

void Uart_Inquire_485Addr(USART_TypeDef* USARTx);
	
unsigned char Wait485SensorAck(void);

void SetSensorRunFlag(unsigned char isTrue);

#endif


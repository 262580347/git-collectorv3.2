#ifndef		_LOGSTORE_H_
#define		_LOGSTORE_H_

#define		LOG_CODE_CLEAR				0XFF		//清错误码
#define		LOG_CODE_NOMODULE			0		//模块不存在
#define		LOG_CODE_WEAKSIGNAL			1		//信号弱
#define		LOG_CODE_UNATTACH			2		//附着不上
#define		LOG_CODE_TIMEOUT			3		//连接超时
#define		LOG_CODE_SUCCESS			4		//上报数据成功
#define		LOG_CODE_HARDERR			5		//硬件错误
#define		LOG_CODE_NOSERVERACK		6		//没有服务器应答
#define		LOG_CODE_NO_CARD			7		//没有SIM卡


#define		LOG_CODE_RETRY				8		//联网重试
#define		LOG_CODE_GATEWAYNOACK		9		//网关无应答
#define		LOG_CODE_GATEWAYNOLINK		10		//网关不在线
#define		LOG_CODE_SENSORERR			11		//传感器数据错误
#define		LOG_CODE_START				12		//第一次上电
#define		LOG_CODE_UNDEFINE			13		//运营商未识别
#define		LOG_CODE_GPS_OK				14		//定位成功
#define		LOG_CODE_GPS_FAIL			15		//定位失败



void LogStorePointerInit(void);

void SetLogErrCode(unsigned short ErrCode);

void ClearLogErrCode(unsigned short ErrCode);

void StoreOperationalData(void);

void ShowLogContent(void);

void CommunicatinLogInit(void);	//通信断线记录初始化

void WriteCommunicationSuccessCount(void);

void WriteCommunicationCount(void);

void ClearCommunicationCount(void);

void SetCSQValue(unsigned short data);

void SetLinkNetTime(unsigned short Time);

unsigned short GetCSQValue(void);

unsigned short GetLinkNetTime(void);

#endif


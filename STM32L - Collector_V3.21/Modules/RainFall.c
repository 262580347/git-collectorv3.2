#include "main.h"
#include "RainFall.h"

PLUSE_SAVE	Pluse_Save;
PLUSE_SAVE	*p_Pluse;

static u8 s_BudData[20];
static u8 UnionPluseFlag = FALSE;

void SetUnionPluse(unsigned char IsTrue)
{
	UnionPluseFlag = IsTrue;
}

void InitPluseCount(void)
{
	p_Pluse = &Pluse_Save;
	
	if (Check_Area_Valid(RAINFALL_COUNT_ADDR))
	{
		EEPROM_ReadBytes(RAINFALL_COUNT_ADDR, s_BudData, sizeof(PLUSE_SAVE));
		
		p_Pluse = (PLUSE_SAVE *)s_BudData;
		
		Pluse_Save.PluseCount1 = p_Pluse->PluseCount1;
		Pluse_Save.PluseCount2 = p_Pluse->PluseCount2;		
	}
	else
	{
		Pluse_Save.Magic 		= 0x55;
		Pluse_Save.Length 		= sizeof(PLUSE_SAVE) - 4;
		Pluse_Save.PluseCount1 	= 0;
		Pluse_Save.PluseCount2 	= 0;
		Pluse_Save.Chksum = Calc_Checksum((unsigned char *)&Pluse_Save.PluseCount1, Pluse_Save.Length);
		EEPROM_WriteBytes(RAINFALL_COUNT_ADDR, (unsigned char *)&Pluse_Save, sizeof(PLUSE_SAVE));
		u1_printf("\r\n Init Pluse Count\r\n");
	}
}

unsigned int GetRainfall(void)
{
	u32 Pluse = 0xEEEEEEEE;
	
	if (Check_Area_Valid(RAINFALL_COUNT_ADDR))
	{
		EEPROM_ReadBytes(RAINFALL_COUNT_ADDR, s_BudData, sizeof(PLUSE_SAVE));
		
		p_Pluse = (PLUSE_SAVE *)s_BudData;
		
		Pluse_Save.PluseCount1 = p_Pluse->PluseCount1;
		
		Pluse = Pluse_Save.PluseCount1;
	}
	
	return Pluse;
}

unsigned int GetRainfall2(void)
{
	u32 Pluse = 0xEEEEEEEE;
	
	if (Check_Area_Valid(RAINFALL_COUNT_ADDR))
	{
		EEPROM_ReadBytes(RAINFALL_COUNT_ADDR, s_BudData, sizeof(PLUSE_SAVE));
		
		p_Pluse = (PLUSE_SAVE *)s_BudData;
		
		Pluse_Save.PluseCount2 = p_Pluse->PluseCount2;
		
		Pluse = Pluse_Save.PluseCount2;
	}
	
	return Pluse;
}

void ClearPluseCount1(void)
{
	if (Check_Area_Valid(RAINFALL_COUNT_ADDR))
	{
		EEPROM_ReadBytes(RAINFALL_COUNT_ADDR, s_BudData, sizeof(PLUSE_SAVE));
		
		p_Pluse = (PLUSE_SAVE *)s_BudData;
		
		Pluse_Save.PluseCount1 = 0;
		Pluse_Save.PluseCount2 = p_Pluse->PluseCount2;
		
		Pluse_Save.Chksum = Calc_Checksum((unsigned char *)&Pluse_Save.PluseCount1, Pluse_Save.Length);
		EEPROM_WriteBytes(RAINFALL_COUNT_ADDR, (unsigned char *)&Pluse_Save, sizeof(PLUSE_SAVE));
		u1_printf("\r\n Clear Pluse Count 1\r\n");
	}
}

void ClearPluseCount2(void)
{
	if (Check_Area_Valid(RAINFALL_COUNT_ADDR))
	{
		EEPROM_ReadBytes(RAINFALL_COUNT_ADDR, s_BudData, sizeof(PLUSE_SAVE));
		
		p_Pluse = (PLUSE_SAVE *)s_BudData;
		
		Pluse_Save.PluseCount1 = p_Pluse->PluseCount1;
		Pluse_Save.PluseCount2 = 0;
		
		Pluse_Save.Chksum = Calc_Checksum((unsigned char *)&Pluse_Save.PluseCount1, Pluse_Save.Length);
		EEPROM_WriteBytes(RAINFALL_COUNT_ADDR, (unsigned char *)&Pluse_Save, sizeof(PLUSE_SAVE));
		u1_printf("\r\n Clear Pluse Count 2\r\n");
	}
}


void RainfallSensorProcess(void)
{
	static u32 s_LastRTCTime = 0, s_LastRTCTime2, s_LastRTCTime3, Pluse1 = 0, Pluse2 = 0;
	static u8 Pluse1Flag = FALSE, Pluse2Flag = FALSE;
	
	if(g_ExtiRainfallFlag)
	{
		g_ExtiRainfallFlag = FALSE;
		
		USART1_Config(115200);	

		DMA_Config();
		
		TIM4_Init(10, SYS_CLOCK);
		
		EXTI17_Reset();		
		
//		RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);		
//		u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
	
		if(DifferenceOfRTCTime(GetRTCSecond(), s_LastRTCTime) > 0)
		{
			s_LastRTCTime = GetRTCSecond();

			if (Check_Area_Valid(RAINFALL_COUNT_ADDR))
			{
				EEPROM_ReadBytes(RAINFALL_COUNT_ADDR, s_BudData, sizeof(PLUSE_SAVE));
				
				p_Pluse = (PLUSE_SAVE *)s_BudData;
				
				Pluse_Save.Magic 		= 0x55;
				Pluse_Save.Length 		= sizeof(PLUSE_SAVE) - 4;
				Pluse_Save.PluseCount1 = p_Pluse->PluseCount1;
				Pluse_Save.PluseCount2 = p_Pluse->PluseCount2;
				Pluse_Save.PluseCount1++;
				
				Pluse_Save.Chksum = Calc_Checksum((unsigned char *)&Pluse_Save.PluseCount1, Pluse_Save.Length);
				EEPROM_WriteBytes(RAINFALL_COUNT_ADDR, (unsigned char *)&Pluse_Save, sizeof(PLUSE_SAVE));
				u1_printf("\r\n Pluse1:%d\r\n", Pluse_Save.PluseCount1);
				if(UnionPluseFlag)
				{
					Pluse1Flag = TRUE;
					s_LastRTCTime3 = GetRTCSecond();
				}
			}
			else
			{
				u1_printf(" Pluse 1 CRC Err\r\n");
			}			
		}
		else
		{
		}
	}	
	
	if(g_ExtiRainfallFlag2)
	{
		g_ExtiRainfallFlag2 = FALSE;
		
		USART1_Config(115200);	
		
		DMA_Config();
		
		TIM4_Init(10, SYS_CLOCK);
		
		EXTI17_Reset();		
		
//		RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);		
//		u1_printf("\r\n [%02d:%02d:%02d] ", RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
	
		if(DifferenceOfRTCTime(GetRTCSecond(), s_LastRTCTime2) > 0)
		{
			s_LastRTCTime2 = GetRTCSecond();
			
			if (Check_Area_Valid(RAINFALL_COUNT_ADDR))
			{
				EEPROM_ReadBytes(RAINFALL_COUNT_ADDR, s_BudData, sizeof(PLUSE_SAVE));
				
				p_Pluse = (PLUSE_SAVE *)s_BudData;
				
				Pluse_Save.Magic 		= 0x55;
				Pluse_Save.Length 		= sizeof(PLUSE_SAVE) - 4;
				Pluse_Save.PluseCount1 = p_Pluse->PluseCount1;
				Pluse_Save.PluseCount2 = p_Pluse->PluseCount2;
				Pluse_Save.PluseCount2++;
				
				Pluse_Save.Chksum = Calc_Checksum((unsigned char *)&Pluse_Save.PluseCount1, Pluse_Save.Length);
				EEPROM_WriteBytes(RAINFALL_COUNT_ADDR, (unsigned char *)&Pluse_Save, sizeof(PLUSE_SAVE));
				u1_printf("\r\n Pluse2:%d\r\n", Pluse_Save.PluseCount2);
				if(UnionPluseFlag)
				{
					Pluse2Flag = TRUE;
					s_LastRTCTime3 = GetRTCSecond();
				}
			}
			else
			{
				u1_printf(" Pluse 2 CRC Err\r\n");
			}
		}
		else
		{
		}

	}
	
	if(UnionPluseFlag)
	{
		if((Pluse1Flag == TRUE) || (Pluse2Flag == TRUE))
		{
			
			if(DifferenceOfRTCTime(GetRTCSecond(), s_LastRTCTime3) <= 2)
			{
				if(Pluse2Flag == TRUE && Pluse1Flag == TRUE)
				{
					Pluse2Flag = FALSE;
					Pluse1Flag = FALSE;
				}
			}
			else if(DifferenceOfRTCTime(GetRTCSecond(), s_LastRTCTime3) > 2)	//清除非正常脉冲
			{
			
				Pluse2Flag = FALSE;
				Pluse1Flag = FALSE;
				Pluse1 = GetRainfall();	
				Pluse2 = GetRainfall2();
				if(Pluse1 == Pluse2)	//仅当两通道脉冲数相等
				{
					//脉冲正常
				}
				else if(Pluse1 > Pluse2)
				{
					EEPROM_ReadBytes(RAINFALL_COUNT_ADDR, s_BudData, sizeof(PLUSE_SAVE));
					
					p_Pluse = (PLUSE_SAVE *)s_BudData;
					
					Pluse_Save.Magic 		= 0x55;
					Pluse_Save.Length 		= sizeof(PLUSE_SAVE) - 4;
					Pluse_Save.PluseCount1 = p_Pluse->PluseCount2;
					Pluse_Save.PluseCount2 = p_Pluse->PluseCount2;
					
					Pluse_Save.Chksum = Calc_Checksum((unsigned char *)&Pluse_Save.PluseCount1, Pluse_Save.Length);
					EEPROM_WriteBytes(RAINFALL_COUNT_ADDR, (unsigned char *)&Pluse_Save, sizeof(PLUSE_SAVE));
				}
				else if(Pluse1 < Pluse2)
				{
					EEPROM_ReadBytes(RAINFALL_COUNT_ADDR, s_BudData, sizeof(PLUSE_SAVE));
					
					p_Pluse = (PLUSE_SAVE *)s_BudData;
					
					Pluse_Save.Magic 		= 0x55;
					Pluse_Save.Length 		= sizeof(PLUSE_SAVE) - 4;
					Pluse_Save.PluseCount1 = p_Pluse->PluseCount1;
					Pluse_Save.PluseCount2 = p_Pluse->PluseCount1;
					
					Pluse_Save.Chksum = Calc_Checksum((unsigned char *)&Pluse_Save.PluseCount1, Pluse_Save.Length);
					EEPROM_WriteBytes(RAINFALL_COUNT_ADDR, (unsigned char *)&Pluse_Save, sizeof(PLUSE_SAVE));
				}
			}
		}
	}

}












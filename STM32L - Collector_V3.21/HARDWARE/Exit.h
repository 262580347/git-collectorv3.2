#ifndef		_EXIT_H_
#define		_EXIT_H_

#define  	EXTI_RUNMODE_LINE					EXTI_Line1
#define	 	EXTI_RUNMODE_SOURCE					EXTI_PinSource1
#define		EXTI_RUNMODE_IRQ					EXTI1_IRQn
#define		EXTI_RUNMODE_GPIO_PIN				GPIO_Pin_1
#define		EXTI_RUNMODE_GPIO_TYPE				GPIOA
#define		EXTI_RUNMODE_PORTSOURCEGPIO_TYPE	EXTI_PortSourceGPIOA

void EXTI_RunMode_Config(void);

void EXTI5_Config(void);

void EXTI17_Reset(void);

void Init_PulseSensorPort(void);

void Init_Pulse2SensorPort(void);

#endif



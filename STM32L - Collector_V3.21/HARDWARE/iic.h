#ifndef __MYIIC_H
#define __MYIIC_H

#define	SDA_PIN		GPIO_Pin_15
#define	SDA_TYPE	GPIOA
#define	SCK_PIN		GPIO_Pin_14
#define	SCK_TYPE	GPIOA

#define SDA_IN()  											\
{															\
	GPIO_InitTypeDef  GPIO_InitStructure;  					\
	GPIO_InitStructure.GPIO_Pin = SDA_PIN;					\
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;			\
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;			\
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;		\
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;		\
	GPIO_Init(SDA_TYPE, &GPIO_InitStructure);			\
}
#define SDA_OUT()   										\
{															\
	GPIO_InitTypeDef  GPIO_InitStructure;  					\
	GPIO_InitStructure.GPIO_Pin = SDA_PIN;					\
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;			\
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;			\
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;		\
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;		\
	GPIO_Init(SDA_TYPE, &GPIO_InitStructure);			\
}	

#define IIC_SCK_HIGH()    	GPIO_SetBits(SCK_TYPE, SCK_PIN)
#define IIC_SCK_LOW() 		GPIO_ResetBits(SCK_TYPE, SCK_PIN)
#define IIC_SDA_HIGH()    	GPIO_SetBits(SDA_TYPE, SDA_PIN)
#define IIC_SDA_LOW() 		GPIO_ResetBits(SDA_TYPE, SDA_PIN)
#define READ_SDA()   		GPIO_ReadInputDataBit(SDA_TYPE, SDA_PIN)

#define	IIC_DELAY()		delay_us(1)

//IIC所有操作函数
void IIC_Init(void);                //初始化IIC的IO口				 
void IIC_Start(void);				//发送IIC开始信号
void IIC_Stop(void);	  			//发送IIC停止信号
void IIC_Send_Byte(unsigned char txd);			//IIC发送一个字节
unsigned char IIC_Read_Byte(unsigned char ack);//IIC读取一个字节
unsigned char IIC_Wait_Ack(void); 				//IIC等待ACK信号
void IIC_Ack(void);					//IIC发送ACK信号
void IIC_NAck(void);				//IIC不发送ACK信号

void IIC_Write_One_Byte(unsigned char daddr,unsigned char addr,unsigned char data);
unsigned char IIC_Read_One_Byte(unsigned char daddr,unsigned char addr);	  
#endif

















#include "iic.h"
#include "main.h"

//初始化IIC
void IIC_Init(void)
{					     
	GPIO_InitTypeDef  GPIO_InitStructure;  	
	
	GPIO_InitStructure.GPIO_Pin = SDA_PIN;					
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;			
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;			
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;		
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;		
	GPIO_Init(SDA_TYPE, &GPIO_InitStructure);		

	GPIO_InitStructure.GPIO_Pin = SCK_PIN;					
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;			
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;			
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;		
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;		
	GPIO_Init(SCK_TYPE, &GPIO_InitStructure);	

	
	GPIO_SetBits(SDA_TYPE, SDA_PIN);
	GPIO_SetBits(SCK_TYPE, SCK_PIN);
}
//产生IIC起始信号
void IIC_Start(void)
{
	SDA_OUT();     //sda线输出
	IIC_SDA_HIGH();	  	  
	IIC_SCK_HIGH();
	IIC_DELAY();
 	IIC_SDA_LOW();//START:when CLK is high,DATA change form high to low 
	IIC_DELAY();
	IIC_SCK_LOW();//钳住I2C总线，准备发送或接收数据 
}	  
//产生IIC停止信号
void IIC_Stop(void)
{
	SDA_OUT();//sda线输出
	IIC_SCK_LOW();
	IIC_SDA_LOW();//STOP:when CLK is high DATA change form low to high
 	IIC_DELAY();
	IIC_SCK_HIGH(); 
	IIC_DELAY();	//GYS
	IIC_SDA_HIGH();//发送I2C总线结束信号
	IIC_DELAY();							   	
}
//等待应答信号到来
//返回值：1，接收应答失败
//        0，接收应答成功
u8 IIC_Wait_Ack(void)
{
	u8 ucErrTime=0;
	SDA_IN();      //SDA设置为输入  
	IIC_SDA_HIGH();
	IIC_DELAY();	   
	IIC_SCK_HIGH();
	IIC_DELAY();	 
	while(READ_SDA())
	{
		ucErrTime++;
		if(ucErrTime>250)
		{
			IIC_Stop();
			return 1;
		}
	}
	IIC_SCK_LOW();//时钟输出0 	   
	return 0;  
} 
//产生ACK应答
void IIC_Ack(void)
{
	IIC_SCK_LOW();
	SDA_OUT();
	IIC_SDA_LOW();
	IIC_DELAY();
	IIC_SCK_HIGH();
	IIC_DELAY();
	IIC_SCK_LOW();
	IIC_DELAY();
}
//不产生ACK应答		    
void IIC_NAck(void)
{
	IIC_SCK_LOW();
	SDA_OUT();
	IIC_SDA_HIGH();
	IIC_DELAY();
	IIC_SCK_HIGH();
	IIC_DELAY();
	IIC_SCK_LOW();
	IIC_DELAY();
}					 				     
//IIC发送一个字节
//返回从机有无应答
//1，有应答
//0，无应答			  
void IIC_Send_Byte(u8 txd)
{                        
    u8 t;   
	SDA_OUT(); 	    
    IIC_SCK_LOW();//拉低时钟开始数据传输
    for(t=0;t<8;t++)
    {              
        //IIC_SDA=(txd&0x80)>>7;
		if((txd&0x80)>>7)
			IIC_SDA_HIGH();
		else
			IIC_SDA_LOW();
		txd<<=1; 	  
		IIC_DELAY();   //对TEA5767这三个延时都是必须的
		IIC_SCK_HIGH();
		IIC_DELAY(); 
		IIC_SCK_LOW();	
		IIC_DELAY();
    }	 
} 	    
//读1个字节，ack=1时，发送ACK，ack=0，发送nACK   
u8 IIC_Read_Byte(unsigned char ack)
{
	unsigned char i,receive=0;
	SDA_IN();//SDA设置为输入
    for(i=0;i<8;i++ )
	{
        IIC_SCK_LOW(); 
        IIC_DELAY();
		IIC_SCK_HIGH();
        receive<<=1;
        if(READ_SDA())receive++;   
		delay_us(1); 
    }					 
    if (!ack)
        IIC_NAck();//发送nACK
    else
        IIC_Ack(); //发送ACK   
    return receive;
}



























